﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }
    public AudioSource GameMusicManager;
    public AudioClip Night;
    public AudioClip Adventure, deadAudio;
    private AudioSource source;
    public AudioClip enemyDeathSoundAudio;
    public AudioClip enemyDeathSoundBirdAudio;
    public AudioClip BossDeathSoundAudio;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        GameMusicManager.Stop();
        GameMusicManager.clip = Adventure;
        GameMusicManager.Play();

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    public bool HasGlasses = false;
    public bool HasJetPack = false;
    public GameOverScreen gameOverScreen, winScreen;
    public GameObject jetpack, glasses;
    public float Speed;
    public int Metres, Lifes,MaxLifes, Coin = 0,Score,Jumps=0, Shots=0, bullets = 1;

    public Zona[] Zones;
    public ZonaPredefinida[] ZonesPredefinides;

    public GameObject[] Enemies, Coins;

    private int zonaActual = 0;
    private int zonaPredefinidaActual = 0;
    private int plataformaPredefinidaActual = 0;
    private int alturaAnterior;
    private GameObject plataformaGenerada;
    [SerializeField]
    private int actualEnemy = 0, actualCoins = 0;

    internal LevelSpawnController levelSpawn = null;
    internal EnemySpawnerController enemySpawn = null;
    internal CoinsSpawnController coinSpawn = null;

    // Start is called before the first frame update
    void Start()
    {
        Lifes = GameManager.Instance.Lifes;
        MaxLifes=GameManager.Instance.Lifes;
        Speed = 1;
    }

    // Update is called once per frame
    void Update()
    {
        ChangeSpeed();
        if (Lifes == 0)
        {
            PlayerDead(false);
            Lifes--;
        }

    }

    public IEnumerator powerupTime (){
        glasses.SetActive(true);
        yield return new WaitForSeconds(GameManager.Instance.PowerUpGlassesTime);
        
        HasGlasses = false;
        glasses.SetActive(false);
    }

    public IEnumerator powerupTime2()
    {
        yield return new WaitForSeconds(GameManager.Instance.PowerUpJetpackTime);
        jetpack.SetActive(false);
        GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>().gravityScale = 1;
        HasJetPack = false;

    }

    public IEnumerator powerupTime3()
    {
        yield return new WaitForSeconds(GameManager.Instance.PowerUpBoostBulletTime);
        LevelManager.Instance.bullets = 1;
    }

    public void PlayerDead(bool win)
    {
        source.PlayOneShot(deadAudio, 0.8f);

        if (win)
        {
            winScreen.Setup();
        }
        else
        {
            gameOverScreen.Setup();

        }
        Speed = 0;
        GameMusicManager.Stop();
        GameMusicManager.clip = Night;
        GameMusicManager.Play();
        Destroy(GameObject.FindWithTag("Player"));
        Score =Metres+Coin*10;

        GameManager.Instance.TotalGames++;
        GameManager.Instance.TotalCoins += Coin;
        GameManager.Instance.Coins += Coin;
        GameManager.Instance.TotalDistance += Metres;
        GameManager.Instance.TotalScore += Score;
        GameManager.Instance.TotalJumps += Jumps;
        GameManager.Instance.TotalShots += Shots;
        if (GameManager.Instance.MaxCoins < Coin)
        {
            GameManager.Instance.MaxCoins = Coin;
        }
        if (GameManager.Instance.MaxDistance < Metres)
        {
            GameManager.Instance.MaxDistance = Metres;
        }
        if (GameManager.Instance.MaxScore < Score)
        {
            GameManager.Instance.MaxScore = Score;
        }
        if (GameManager.Instance.MaxJumps < Jumps)
        {
            GameManager.Instance.MaxJumps = Jumps;
        }
        if (GameManager.Instance.MaxShots < Shots)
        {
            GameManager.Instance.MaxShots = Shots;
        }
    }
    public void ChangeSpeed()
    {
        if (0 < Speed&& Speed < 5)
        {
            Speed += Time.deltaTime / 5;
        }
    }
    //Coins
    public List<GameObject> AddNewCoins()
    {
        List<GameObject> newCoins = new List<GameObject>();
        //Revisar si s'ha arribat a una distància on han d'apareixer nous coins
        if (Coins.Length > actualCoins)
        {
            if (Coins[actualCoins].GetComponent<EnemyAppears>().InitalDistance < Metres)
            {
                newCoins.Add(Coins[actualCoins]);
                actualCoins++;
            }
        }
        return newCoins;
    }
    //Enemies
    public List<GameObject> AddNewEnemies()
    {
        List<GameObject> newEnemies= new List<GameObject>();
        //Revisar si s'ha arribat a una distància on han d'apareixer nous enemics
        if (Enemies.Length > actualEnemy)
        {
            if (Enemies[actualEnemy].GetComponent<EnemyAppears>().InitalDistance < Metres)
            {
                newEnemies.Add(Enemies[actualEnemy]);
                actualEnemy ++;
            }
        }
        return newEnemies;
    }
    public int AlturaPlataformaActual()
    {
        //Mira l'altura de la plataforma actual+1, en el cas que estigui en un forat retorna -20
        if (levelSpawn != null)
        {
            if (levelSpawn.plataforma)
            {
                return levelSpawn.plataformaActual.Altura+1;
            }
        }
    return -20;
    }
    //Plataformes
    public PlataformaPredefinida NextPlatform()
    {
        PlataformaPredefinida plataformaAMostrar;
        //Mirar si s'ha arribat a una ZonaPredefinida
        if (zonaPredefinidaActual < ZonesPredefinides.Length && ZonesPredefinides[zonaPredefinidaActual].MetresInici >= Metres)
        {
            //Agafar la següent PlataformaPredefinida
            plataformaAMostrar = ZonesPredefinides[zonaPredefinidaActual].CamiBase[plataformaPredefinidaActual];
            plataformaPredefinidaActual++;
            alturaAnterior = plataformaAMostrar.Altura;
            //Mirar si s'ha acabat la ZonaPredefinida
            if (ZonesPredefinides[zonaPredefinidaActual].CamiBase.Count <= plataformaPredefinidaActual)
            {
                plataformaPredefinidaActual = 0;
                zonaPredefinidaActual++;
            }
        }
        else
        {
            //Mirar si comença la següent Zona
            if (zonaActual < Zones.Length-1 &&Zones[zonaActual + 1].MetresInici < Metres)
            {
                zonaActual++;
            }
            //Generar nova plataforma
            plataformaAMostrar = generarPlataforma();
        }
        //Retorna una PlataformaPredefinida
        return plataformaAMostrar;
    }
    public int NextForat()
    {
        //Retorna el tamany del següent forat
        int forat= Random.Range(Zones[zonaActual].MinForat, Zones[zonaActual].MaxForat);
        return forat;
    }
    public bool ProbabilitatForat()
    {
        //Aleatoriament desideix si hi haurà forat o no
        if(zonaPredefinidaActual < ZonesPredefinides.Length && ZonesPredefinides[zonaPredefinidaActual].MetresInici >= Metres)
        {
            return false;
        }
        else
        {
            if (Random.Range(0, 100) <= Zones[zonaActual].ProbabilitatForat)
            {
                return true;
            }
            return false;
        }
         
    }
    public PlataformaPredefinida generarPlataforma()
    {
        //Agafar la Plataforma de la Zona actual
        Plataforma plataforma = Zones[zonaActual].PlataformaBase;
        //Aleatoriament desideix sl'amplada de la següent plataforma
        int amplada = Random.Range(plataforma.MinAmplada, plataforma.MaxAmplada+1);
        //Mirar si augmenta o disminueix d'aluta
            //Fer el canvi d'altura
        if (alturaAnterior - plataforma.MinCanviAlturaAvall < plataforma.MinAltura)
        {
            canviAltura(true,plataforma);
        }
        else if (alturaAnterior + plataforma.MinCanviAlturaAmunt > plataforma.MaxAltura)
        {
            canviAltura(false,plataforma);
        }
        else if (Random.Range(0, 2) < 1)
        {
            canviAltura(true,plataforma);
        }
        else
        {
            canviAltura(false,plataforma);
        }
        //Destruir el GameObject on es guardava la plataforma anterior
        Destroy(plataformaGenerada);
        //Generar un nou GameObject on guardar la nova plataforma
        plataformaGenerada = new GameObject();
        //Afegir el component PlataformaPredefinia
        plataformaGenerada.AddComponent<PlataformaPredefinida>();
        PlataformaPredefinida plPr = plataformaGenerada.GetComponent<PlataformaPredefinida>();
        //Assignar l'altura, l'amplada i la Plataforma
        plPr.Amplada = amplada;
        plPr.Altura =alturaAnterior;
        plPr.Plataforma = plataforma;
        return plPr;

    }
    public void canviAltura(bool amunt,Plataforma plataforma)
    {
        //Calcula la nova altura respecta a l'anterior
        if (amunt)
        {
            alturaAnterior += Random.Range(plataforma.MinCanviAlturaAmunt, plataforma.MaxCanviAlturaAmunt + 1);
            if (alturaAnterior > plataforma.MaxAltura)
            {
                alturaAnterior = plataforma.MaxAltura;
            }
        }
        else
        {
            alturaAnterior -= Random.Range(plataforma.MinCanviAlturaAvall, plataforma.MaxCanviAlturaAvall + 1);
            if (alturaAnterior < plataforma.MinAltura)
            {
                alturaAnterior = plataforma.MinAltura;
            }
        }
    }

    //Init components
    public void initEnemySpawn()
    {
        //Inicialitza EnemySpawn
        enemySpawn = GameObject.Find("LevelSpawner").GetComponent<EnemySpawnerController>();
    }
    public void initLevelSpawn()
    {
        //Inicialitza LevelSpawn
        levelSpawn = GameObject.Find("LevelSpawner").GetComponent<LevelSpawnController>();
    }
    public void initCoinSpawn()
    {
        //Inicialitza CoinSpawn
        coinSpawn = GameObject.Find("LevelSpawner").GetComponent<CoinsSpawnController>();
    }

    public void enemyDeathSound()
    {
        source.PlayOneShot(enemyDeathSoundAudio, 1);
    }
    public void enemyDeathSoundBird()
    {
        source.PlayOneShot(enemyDeathSoundBirdAudio, 1);
    }

}
