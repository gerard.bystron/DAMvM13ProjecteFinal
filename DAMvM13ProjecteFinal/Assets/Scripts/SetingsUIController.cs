﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetingsUIController : MonoBehaviour
{
    public Slider Volume;
    public GameObject ConfirmationPanel;
    // Start is called before the first frame update
    void Start()
    {
        Volume.value = AudioListener.volume;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AdjustVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
    }
    public void DeleteDataFirstStep()
    {
        ConfirmationPanel.SetActive(true);
    }
    public void DeleteDataSecondtStep(bool confirmation)
    {
        if (confirmation)
        {
            PlayerPrefs.DeleteKey("PowerUpGlassesTime");
            PlayerPrefs.DeleteKey("PowerUpBoostBulletTime");
            PlayerPrefs.DeleteKey("PowerUpJetpackTime");
            PlayerPrefs.DeleteKey("MaxCoins");
            PlayerPrefs.DeleteKey("MaxDistance");
            PlayerPrefs.DeleteKey("MaxScore");
            PlayerPrefs.DeleteKey("MaxJumps");
            PlayerPrefs.DeleteKey("MaxShots");
            PlayerPrefs.DeleteKey("TotalCoins");
            PlayerPrefs.DeleteKey("TotalDistance");
            PlayerPrefs.DeleteKey("TotalScore");
            PlayerPrefs.DeleteKey("TotalJumps");
            PlayerPrefs.DeleteKey("TotalShots");
            PlayerPrefs.DeleteKey("KilledBoss");
            PlayerPrefs.DeleteKey("KilledMushroom");
            PlayerPrefs.DeleteKey("KilledBird");
            PlayerPrefs.DeleteKey("TotalGames");
            PlayerPrefs.DeleteKey("Coins");
            PlayerPrefs.DeleteKey("LifeLevel");
            PlayerPrefs.DeleteKey("GlassesLevel");
            PlayerPrefs.DeleteKey("JetpackLevel");
            PlayerPrefs.DeleteKey("MultiBulletLevel");
            PlayerPrefs.DeleteKey("Lifes");
            PlayerPrefs.Save();
            Application.Quit();
        }
        else
        {
            ConfirmationPanel.SetActive(false);
        }
    }
}
