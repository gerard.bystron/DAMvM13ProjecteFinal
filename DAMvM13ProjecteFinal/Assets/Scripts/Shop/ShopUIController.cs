﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ItemShop
{
    public ItemShopSC InfoItem;
    public Text TextNameItem;
    public Text TextDescriptionItem;
    public Text TextCostItem;
    public Button ButtonBuyItem;
}
public class ShopUIController : MonoBehaviour
{
    public ItemShop[] Items;
    public string MaxLevelDescription;
    public string MaxLevelCost;
    public string CoinsStringExtraText;
    public Text CoinsText;

    // Start is called before the first frame update
    void Start()
    {
        setItem(Items[0], GameManager.Instance.LifeLevel);
        setItem(Items[1], GameManager.Instance.GlassesLevel);
        setItem(Items[2], GameManager.Instance.JetpackLevel);
        setItem(Items[3], GameManager.Instance.MultiBulletLevel);
    }
    private void setItem(ItemShop item, int level)
    {
        if (level == 3)
        {
            item.ButtonBuyItem.GetComponent<Button>().enabled = false;
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = MaxLevelDescription;
            item.TextCostItem.text = MaxLevelCost;
        }
        else if (level == 2)
        {
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description+" "+ (item.InfoItem.Count[3] - item.InfoItem.Count[2]);
            item.TextCostItem.text = item.InfoItem.Cost[2] + CoinsStringExtraText;
        }
        else if (level == 1)
        {
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description + " " + (item.InfoItem.Count[2] - item.InfoItem.Count[1]);
            item.TextCostItem.text = item.InfoItem.Cost[1] + CoinsStringExtraText;
        }
        else
        {
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description + " " + (item.InfoItem.Count[1] - item.InfoItem.Count[0]);
            item.TextCostItem.text = item.InfoItem.Cost[0] + CoinsStringExtraText;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CoinsText.text = GameManager.Instance.Coins + CoinsStringExtraText;
    }

    public void buy(int position)
    {
        int level;
        if (position == 0)
        {
            level= GameManager.Instance.LifeLevel;
        }
        else if (position == 1)
        {
            level = GameManager.Instance.GlassesLevel;
        }
        else if (position == 2)
        {
            level = GameManager.Instance.JetpackLevel;
        }
        else
        {
            level = GameManager.Instance.MultiBulletLevel;
        }
        if (GameManager.Instance.Coins >= Items[position].InfoItem.Cost[level])
        {
            GameManager.Instance.Coins -= Items[position].InfoItem.Cost[level];
            level++;
            if (position == 0)
            {
                GameManager.Instance.LifeLevel++;
                GameManager.Instance.Lifes = Items[position].InfoItem.Count[level];
            }
            else if (position == 1)
            {
                GameManager.Instance.GlassesLevel++;
                GameManager.Instance.PowerUpGlassesTime = Items[position].InfoItem.Count[level];
            }
            else if (position == 2)
            {
                GameManager.Instance.JetpackLevel++;
                GameManager.Instance.PowerUpJetpackTime = Items[position].InfoItem.Count[level];
            }
            else
            {
                GameManager.Instance.MultiBulletLevel++;
                GameManager.Instance.PowerUpBoostBulletTime = Items[position].InfoItem.Count[level];
            }
            Debug.Log(Items[position].InfoItem.Cost.Length+" "+level);
            if (Items[position].InfoItem.Cost.Length <= level)
            {
                Items[position].ButtonBuyItem.GetComponent<Button>().enabled = false;
            }
        }
        setItem(Items[position], level);
    }
}
