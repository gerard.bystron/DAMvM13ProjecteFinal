﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemShop", menuName = "ScriptableObjects/ItemShop", order = 1)]
public class ItemShopSC : ScriptableObject
{
    public string Name;
    public string Description;
    public int[] Count;
    public int[] Cost;
}
