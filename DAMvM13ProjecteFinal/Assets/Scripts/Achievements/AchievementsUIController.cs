﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Achievement
{
    public AchievementSC InfoAchievement;
    public Text TextAchievement;
    public Image PanelAchievement;
}
public class AchievementsUIController : MonoBehaviour
{
    public Achievement[] AchievementsColumn1;
    public Achievement[] AchievementsColumn2;
    public Achievement[] AchievementsColumn3;
    public Achievement[] AchievementsColumn4;
    public Achievement[] AchievementsColumn5;
    public Achievement[] AchievementsColumn6;
    public Color NoneColor;
    public Color Level1Color;
    public Color Level2Color;
    public Color Level3Color;
    // Start is called before the first frame update
    void Start()
    {
        setAchevement(AchievementsColumn1[0], GameManager.Instance.MaxScore);
        setAchevement(AchievementsColumn1[1], GameManager.Instance.TotalScore);
        setAchevement(AchievementsColumn1[2], GameManager.Instance.KilledMushroom);
        setAchevement(AchievementsColumn2[0], GameManager.Instance.MaxDistance);
        setAchevement(AchievementsColumn2[1], GameManager.Instance.TotalDistance);
        setAchevement(AchievementsColumn2[2], GameManager.Instance.KilledBird);
        setAchevement(AchievementsColumn3[0], GameManager.Instance.MaxCoins);
        setAchevement(AchievementsColumn3[1], GameManager.Instance.TotalCoins);
        setAchevement(AchievementsColumn3[2], GameManager.Instance.KilledBoss);
        setAchevement(AchievementsColumn4[0], GameManager.Instance.MaxJumps);
        setAchevement(AchievementsColumn4[1], GameManager.Instance.TotalJumps);
        setAchevement(AchievementsColumn5[0], GameManager.Instance.MaxShots);
        setAchevement(AchievementsColumn5[1], GameManager.Instance.TotalShots);
        setAchevement(AchievementsColumn6[0], GameManager.Instance.TotalGames);
    }
    private void setAchevement(Achievement achievement, int value)
    {
        if (value >= achievement.InfoAchievement.Level3Count)
        {
            Debug.Log("Gold");
            achievement.PanelAchievement.color = Level3Color;
            achievement.TextAchievement.text = achievement.InfoAchievement.Name + "\n" + value;
        }
        else if (value >= achievement.InfoAchievement.Level2Count)
        {
            Debug.Log("Silver");
            achievement.PanelAchievement.color = Level2Color;
            achievement.TextAchievement.text = achievement.InfoAchievement.Name + "\n" + value + "\n" + achievement.InfoAchievement.Level3Count;
        }
        else if (value >= achievement.InfoAchievement.Level1Count)
        {
            Debug.Log("Bronze");
            achievement.PanelAchievement.color = Level1Color;
            achievement.TextAchievement.text = achievement.InfoAchievement.Name + "\n" + value + "\n" + achievement.InfoAchievement.Level2Count;
        }
        else
        {
            Debug.Log("None");
            achievement.PanelAchievement.color = NoneColor;
            achievement.TextAchievement.text = achievement.InfoAchievement.Name + "\n" + value + "\n" + achievement.InfoAchievement.Level1Count;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
