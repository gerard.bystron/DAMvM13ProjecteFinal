﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Achievement", menuName = "ScriptableObjects/Achievement", order = 1)]
public class AchievementSC : ScriptableObject
{
    public string Name;
    public int Level1Count;
    public int Level2Count;
    public int Level3Count;
}
