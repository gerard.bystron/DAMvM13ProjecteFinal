﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour
{
    [Range(0.1f, 1F)]
    public float ScrollSpeed = 1;
    public float ScrollOfSet;

    Vector2 StartPos;

    

    // Start is called before the first frame update
    void Start()
    {
        StartPos = new Vector2(Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x,transform.position.y);    
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = StartPos + Vector2.right * Mathf.Repeat(Time.timeSinceLevelLoad * -ScrollSpeed * LevelManager.Instance.Speed, ScrollOfSet);

    }
}
