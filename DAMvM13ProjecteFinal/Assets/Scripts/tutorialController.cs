﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialController : MonoBehaviour
{
    public GameObject dialogo1, dialogo2, dialogo3, dialogo4, dialogo5, dialogo6;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(tutorial());
    }

    // Update is called once per frame
    void Update()
    {

    }
    public IEnumerator tutorial()
    {
        yield return new WaitForSeconds(0.5f);
        dialogo1.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo1.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        dialogo2.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo2.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo3.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo3.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo4.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo4.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo5.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo5.SetActive(false);
    }
}
