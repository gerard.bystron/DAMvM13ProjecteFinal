﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class birdAttackScript : MonoBehaviour
{
    Transform PlayerPos;
    public GameObject Egg;
    bool eggThowed = false;
    public AudioClip EggThrowAudio;
    private AudioSource source;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        PlayerPos = GameObject.FindGameObjectWithTag("Player").transform;
        this.gameObject.transform.position = new Vector3 (gameObject.transform.position.x, 3,0);
        
        
    }

    // The birdo compares his coordinates with the player's, throws an egg if it's on top of him.
    void Update()
    {
        if (PlayerPos != null)
        {
            if (gameObject.transform.position.x <= PlayerPos.position.x+0.3 && !eggThowed)
            {
                source.PlayOneShot(EggThrowAudio, 1);
                eggThowed = true;
                Instantiate(Egg, gameObject.transform.position, Quaternion.identity);
            }
        }
    }
}
