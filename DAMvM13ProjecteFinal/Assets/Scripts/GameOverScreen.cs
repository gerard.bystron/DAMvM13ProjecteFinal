﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    public Text distanceText, coinsText, pointsText;

    public void Setup()
    {
        gameObject.SetActive(true);
        distanceText.text = "DISTANCE : " + LevelManager.Instance.Metres.ToString()+" M";
        coinsText.text = "COINS : " + LevelManager.Instance.Coin.ToString();
        pointsText.text = "TOTAL SCORE : "+(LevelManager.Instance.Metres + LevelManager.Instance.Coin * 10).ToString();
    }
    public void MenuButton() {
        SceneManager.LoadScene("MenuScene");
    }
    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
