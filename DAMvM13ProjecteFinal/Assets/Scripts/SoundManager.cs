﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public AudioClip audio1;
    public AudioClip audio2;
    public AudioClip audio3;
    public AudioClip audio4;

    private AudioSource source;

    void Awake()
    {

        source = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playSelectSound()
    {
        source.PlayOneShot(audio1, 1 );
    }
    public void playSelectSound2()
    {
        source.PlayOneShot(audio3, 1);
    }
    public void playCancelSound()
    {
        source.PlayOneShot(audio2, 1);
    }
    public void UpgradeSound()
    {
        source.PlayOneShot(audio4, 1);
    }

   
}
