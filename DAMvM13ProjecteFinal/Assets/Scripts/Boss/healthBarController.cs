﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthBarController : MonoBehaviour
{
    private Image HealthBar;
    public float currentHealth;
    private float maxHealth;
    Boss1 boss;
    // Start is called before the first frame update
    void Start()
    {
        HealthBar = GetComponent<Image>();
        boss = FindObjectOfType<Boss1>();
        maxHealth = 50;
    }

    // Update is called once per frame
    void Update()
    {
        currentHealth = boss.BossLife;
        HealthBar.fillAmount = currentHealth / maxHealth;
    }
}
