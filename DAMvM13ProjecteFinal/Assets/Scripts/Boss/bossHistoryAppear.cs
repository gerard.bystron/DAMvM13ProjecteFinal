﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class bossHistoryAppear : MonoBehaviour
{

    public GameObject PrefabBoss, PortalPrefab, player ,dialogo1, dialogo2, dialogo3, dialogo4, dialogo5, dialogo6;
    private GameObject portal, boss;
    public float speed;
    private bool hasAppear = false, portalIsDestoyed = false, dialogActive = false, dialogEnded;
    
    // Start is called before the first frame update
    void Start()
    {

        
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasAppear)
        {
            hasAppear = true;
            portal = Instantiate(PortalPrefab, new Vector2(8.5f, 2.5f), Quaternion.identity);
            boss = Instantiate(PrefabBoss, new Vector2(11, 3.75f), Quaternion.identity);
        }
        if (portalIsDestoyed == false)
        {
            if (portal.GetComponent<Transform>().position.x - 3 < boss.GetComponent<Transform>().position.x)
            {
                speed = 5 * Time.deltaTime;
                boss.GetComponent<Transform>().position = new Vector3(boss.GetComponent<Transform>().position.x - speed, boss.GetComponent<Transform>().position.y, boss.GetComponent<Transform>().position.z);
            }
            else
            {
                portalIsDestoyed = true;
                
            }
        }
        else
        {
            if (!dialogActive)
            {
                StartCoroutine(primerDialogo());
                dialogActive = true;
               
                        
            }
            if (dialogEnded) 
                {
                changeScene();
                }
        }
        
    }
    public void changeScene()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public IEnumerator primerDialogo() 
    {
        yield return new WaitForSeconds(1f);
        dialogo1.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo1.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo2.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo2.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo3.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo3.SetActive(false);
        //TO-DO ANIM MONKEY WAKING UP
        yield return new WaitForSeconds(1f);
        player.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(1f);
        player.GetComponent<Animator>().SetBool("finishedJump", true); 
        dialogo4.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo4.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo5.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo5.SetActive(false);
        yield return new WaitForSeconds(1f);
        dialogo6.SetActive(true);
        yield return new WaitForSeconds(2f);
        dialogo6.SetActive(false);
        dialogEnded = true; 

    }
}
