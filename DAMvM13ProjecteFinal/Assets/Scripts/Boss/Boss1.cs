﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1 : Boss
{
    public GameObject Mushroom, BulletPrefab;
    private GameObject bullet;
    public float speed = 30f;
    public float invokeWaitTime;
    public float shotWaitTime;
    private float iWaitTime, sWaitTime;
    private Animator bossAnim;
    //AUDIO
    public AudioClip shroomIvokeaudio, audioBossShoot;
    private AudioSource source;

    public override void Start()
    {
        source = GetComponent<AudioSource>();

        iWaitTime = invokeWaitTime;
        sWaitTime = shotWaitTime;
        bossAnim = gameObject.GetComponent<Animator>();
        State = StateBoss.Appearing;
        FullLife = BossLife;
       
    }
    public override void appearing()
    {
        
        State = StateBoss.Phase1;
    }

    public override void changePhase12()
    {
        makeBossGodMode();
        State = StateBoss.Phase2;
    }

    public override void changePhase23()
    {
        makeBossGodMode();
        State = StateBoss.Phase3;
    }

    public override void dead()
    {
        bossAnim.SetBool("isDead", true);
        

    }
    public void killBoss1()
    {
        LevelManager.Instance.PlayerDead(true);
        GameManager.Instance.KilledBoss++;
        Destroy(gameObject);
    }

    public override void phase1()
    {
        if (nextPhase(1))
        {
            State = StateBoss.ChangePhase12;
        }
        
        spawnMushroom();
        

    }

    public override void phase2()
    {
        if (nextPhase(2))
        {
            State = StateBoss.ChangePhase23;
        }
        shoot();
    }

    public override void phase3()
    {
        if (nextPhase(3))
        {
            

            State = StateBoss.Dead;
        }
        shoot();
        spawnMushroom();

    }
    private void spawnMushroom()
    {
        if (iWaitTime <= 0)
        {
            iWaitTime = invokeWaitTime;
            bossAnim.SetBool("isInvoking", true);
        }
        iWaitTime -= Time.deltaTime;
    }
    private void changeBossAnimEventInvoke()
    {
        bossAnim.SetBool("isInvoking", false);
    }

    private void changeBossAnimEventShoot()
    {
        bossAnim.SetBool("isShooting", false);
    }

    private void changeBossAnimEventDead()
    {
        bossAnim.SetBool("isDead", false);
    }


    private void shoot()
    {
        if (sWaitTime <= 0)
        {
            bossAnim.SetBool("isShooting", true);
            sWaitTime = shotWaitTime;

        }
        sWaitTime -= Time.deltaTime;
    }
    private void createMushroom()
    {
        source.PlayOneShot(shroomIvokeaudio, 1);
        Instantiate(Mushroom, transform.position - new Vector3(1, 1.5f, 0), Quaternion.identity);
    }
    private void createBullet()
    {
        source.PlayOneShot(audioBossShoot, 1);
        Vector2 aim = GameObject.Find("Player").GetComponent<Rigidbody2D>().position - this.GetComponent<Rigidbody2D>().position + new Vector2(1, 1.5f);
        bullet = Instantiate(BulletPrefab, transform.position - new Vector3(1, 1.5f, 0), Quaternion.Euler(0f, 0f, Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg));
        bullet.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(1, 0) * speed, ForceMode2D.Force);
    }
    private bool nextPhase(int phase)
    {
        return (FullLife-((FullLife/3f)*phase) >= BossLife);
    }

    private void makeBossGodMode()
    {
        isGod = true;
        bossAnim.SetBool("isChangingPhase", true);
        StartCoroutine(waitToChangeStage());
    }

    IEnumerator waitToChangeStage()
    {
        yield return new WaitForSeconds(1f);
        isGod = false;
        bossAnim.SetBool("isChangingPhase", false);
    }
}
