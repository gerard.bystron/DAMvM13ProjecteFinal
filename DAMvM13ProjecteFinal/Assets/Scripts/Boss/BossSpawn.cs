﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawn : MonoBehaviour
{
    public GameObject PrefabBoss, PortalPrefab;
    private GameObject portal, boss;
    public GameObject healthBarUI;
    public int MetresAparicio;
    private bool hasAppear = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelManager.Instance.Metres > MetresAparicio)
        {
            if (!hasAppear)
            {
                hasAppear = true;
                portal = Instantiate(PortalPrefab, new Vector2(Camera.main.ScreenToWorldPoint(new Vector2(Screen.width -0.5f, 0)).x, 2.5f), Quaternion.identity);
                boss = Instantiate(PrefabBoss, new Vector2(Camera.main.ScreenToWorldPoint(new Vector2(Screen.width + 1, 0)).x, 3.5f), Quaternion.identity);
                healthBarUI.SetActive(true);
            }
            if(portal.GetComponent<Transform>().position.x - 2 < boss.GetComponent<Transform>().position.x)
            {
                boss.GetComponent<Transform>().position = new Vector3(boss.GetComponent<Transform>().position.x - 0.1f, boss.GetComponent<Transform>().position.y, boss.GetComponent<Transform>().position.z);
            }
            else
            {
                Destroy(this);
            } 
        }
    }
}
