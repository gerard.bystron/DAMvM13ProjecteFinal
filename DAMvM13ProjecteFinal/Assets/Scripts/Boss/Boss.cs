﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateBoss
{
    Nothing,
    Appearing,
    Phase1,
    ChangePhase12,
    Phase2,
    ChangePhase23,
    Phase3,
    Dead
} 
public abstract class Boss : MonoBehaviour
{
    public StateBoss State=StateBoss.Nothing;
    public int BossLife;
    public bool isGod = false;
    protected int FullLife;
    // Start is called before the first frame update
    public virtual void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (State)
        {
            case StateBoss.Appearing:
                appearing();
                break;
            case StateBoss.Phase1:
                phase1();
                break;
            case StateBoss.ChangePhase12:
                changePhase12();
                break;
            case StateBoss.Phase2:
                phase2();
                break;
            case StateBoss.ChangePhase23:
                changePhase23();
                break;
            case StateBoss.Phase3:
                phase3();
                break;
            case StateBoss.Dead:
                dead();
                break;
        }
    }
    public abstract void appearing();
    public abstract void phase1();
    public abstract void changePhase12();
    public abstract void phase2();
    public abstract void changePhase23();
    public abstract void phase3();
    public abstract void dead();
    
    public void lostLife()
    {
        if (!isGod)
        {
            BossLife--;
        }
    }
}
