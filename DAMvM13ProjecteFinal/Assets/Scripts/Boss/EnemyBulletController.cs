﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletController : BulletDestroyer
{
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            Destroy(gameObject);

        }
        else if (collision.gameObject.tag == "Player")
        {
            LevelManager.Instance.Lifes--;
            Destroy(gameObject);
        }
    }
}
