﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKiller : MonoBehaviour
{
   
   
    public Animator PortalAnim;
    // Start is called before the first frame update
    void Awake()
    {
       // source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void killThis() {
        //source.PlayOneShot(enemyDeadSound, 1);
        Destroy(this.gameObject);
    }
    public void cantKill()
    {
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY ;
        gameObject.GetComponent<Rigidbody2D>().freezeRotation=true;
        gameObject.GetComponent<Collider2D>().enabled = false;
    }
    public void closeAnimPortal() 
    {
       PortalAnim.SetBool("isClosingPortal", true);  
    }
    public void disableThis()
    {
        this.gameObject.SetActive(false);
    }
}
