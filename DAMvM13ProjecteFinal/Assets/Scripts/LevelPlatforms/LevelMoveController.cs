﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMoveController : MonoBehaviour
{
    private Transform position;
    // Start is called before the first frame update
    void Start()
    {
        position = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        position.position=new Vector3(position.position.x-Time.deltaTime * LevelManager.Instance.Speed, position.position.y,position.position.z);
    }
}
