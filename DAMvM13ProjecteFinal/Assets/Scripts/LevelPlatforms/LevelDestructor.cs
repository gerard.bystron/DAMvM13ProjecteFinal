﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDestructor : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Platform") || collision.CompareTag("EnemyBird")|| collision.CompareTag("Enemy") || collision.CompareTag("Coin")||collision.CompareTag("BadCoin")||collision.CompareTag("Heart")|| collision.CompareTag("Glasses") || collision.CompareTag("BananaPack") || collision.CompareTag("BoostBullet"))
        {
            Destroy(collision.gameObject);
        }
    }
}
