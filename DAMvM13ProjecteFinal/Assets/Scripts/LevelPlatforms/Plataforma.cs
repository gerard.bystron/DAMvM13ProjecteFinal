﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable()]
public class Plataforma : MonoBehaviour
{
public enum type1
{
    Base,
    Extra1
}
public enum type2
{
    Normal1,
    Flotant1
}

    public type1 tipus1=type1.Base;
    public type2 tipus2=type2.Normal1;
    //Comunes
    public int MinAltura;
    public int MaxAltura;
    public int MinAmplada;
    public int MaxAmplada;

    //Tiles
    public GameObject TopLeft;
    public GameObject Top;
    public GameObject TopRight;
    public GameObject Left;
    public GameObject Center;
    public GameObject Right;
    public GameObject BottomLeft;
    public GameObject Bottom;
    public GameObject BottomRight;

    //Altres
    public int MinCanviAlturaAmunt;
    public int MaxCanviAlturaAmunt;
    public int MinCanviAlturaAvall;
    public int MaxCanviAlturaAvall;
    public int MinDifAlturaABase;
    public int MaxDifAlturaABase;
}
