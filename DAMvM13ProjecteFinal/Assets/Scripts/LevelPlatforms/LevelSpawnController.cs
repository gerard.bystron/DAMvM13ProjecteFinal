﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawnController : MonoBehaviour
{
    public float distancia = 0.6f;
    internal PlataformaPredefinida plataformaActual;
    [SerializeField]
    private float temps=0;
    
    internal bool plataforma;
    private Vector3 positionBase;

    [SerializeField]
    private float posicioX;
    [SerializeField]
    private int ampladaPlataformaActual;

    // Start is called before the first frame update
    void Start()
    {
        //Inicialtzar LevelSpawn al LevelManager
        LevelManager.Instance.initLevelSpawn();
        //Coordenades del mon del limit dret i inferior de la pantalla
        positionBase = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,0,0));
        //Coordenada x del mon del limit esquer de la pantalla
        posicioX = positionBase.x * -1;
        //Crear plataformes de la part esquerra de la pantalla fins a la part dreta
        while (posicioX<positionBase.x + distancia * 4.4f)
        {
            plataformaActual=LevelManager.Instance.NextPlatform();
            printPlataforma();
        }

        ampladaPlataformaActual = plataformaActual.Amplada;
        float dif = 0;
        //Amplada de la part de la plataforma fora de pantalla
        while (posicioX+dif > positionBase.x+distancia*4.4f)
        {
            ampladaPlataformaActual--;
            dif -= distancia;
        }
        ampladaPlataformaActual= plataformaActual.Amplada-ampladaPlataformaActual;
        //Temps d'espera a velocitat 1 per següent possible spawn
        temps = distancia;
    }

    // Update is called once per frame
    void Update()
    {
        //Moure punt spawn respecte a temps transcorrgut i velocitat actual
        posicioX -=Time.deltaTime* LevelManager.Instance.Speed;
        //Mirar si s'ha arribat al següent instant d'spawn
        if (temps <= 0)
        {
            //Augmentar la distància recorreguda
            LevelManager.Instance.Metres++;
            //Mirar si la plataforma actual ha acabat
            if (ampladaPlataformaActual <= 1)
            {
                //Si està en una plataforma mirar si el següent serà un forat o una altre plataforma
                if (plataforma)
                {
                    if (LevelManager.Instance.ProbabilitatForat())
                    {
                        ampladaPlataformaActual = LevelManager.Instance.NextForat();
                        for (int i = 0; i < ampladaPlataformaActual; i++)
                        {
                            posicioX += distancia;
                        }
                        plataforma = false;
                    }
                }
                //Si està en un forat el següent serà una plataforma
                else
                {
                    plataforma = true;
                }

                //Si ha tocat plataforma
                if (plataforma)
                {
                    //Demanar al LevelManager la següent plataforma
                    plataformaActual = LevelManager.Instance.NextPlatform();
                    //Printar la plataforma
                    printPlataforma();
                    ampladaPlataformaActual = plataformaActual.Amplada;
                }
                
            }
            else
            {
                //Si la plataforma no ha acabat disminuir l'amplada d'aquesta
                ampladaPlataformaActual--;
            }
            //Temps d'espera a velocitat 1 per següent possible spawn
            temps += (distancia);
        }
        //Disminuir el temps respecte a temps transcorrgut i la velocitat actual
        temps -= Time.deltaTime * LevelManager.Instance.Speed;
    }

    public void printPlataforma()
    {
        //Segons el tipus de plataforma es printaran uns tiles o un altres
        switch (plataformaActual.Plataforma.tipus2)
        {
            case Plataforma.type2.Normal1:
                //En el cas de que sigui de tipus normal es printa la pròpia plataforma i tota la part inferior
                for (int i = 0; i < plataformaActual.Amplada; i++)
                {
                    if (i == 0)
                    {
                        interior(plataformaActual.Plataforma.Left);
                        top(plataformaActual.Plataforma.TopLeft);
                    }
                    else if (i == plataformaActual.Amplada - 1)
                    {
                        interior(plataformaActual.Plataforma.Right);
                        top(plataformaActual.Plataforma.TopRight);
                    }
                    else
                    {
                        interior(plataformaActual.Plataforma.Center);
                        top(plataformaActual.Plataforma.Top);
                    }
                    //Moure la posició d'spawn respecte al tamany del tile
                    posicioX += distancia;
                }
                break;
            case Plataforma.type2.Flotant1:
                //En el cas de que sigui de tipus flotat nomès es printa la pròpia plataforma
                for (int i=0; i < plataformaActual.Amplada; i++)
                {
                    if (i == 0)
                    {
                        top(plataformaActual.Plataforma.Left);
                    }else if(i== plataformaActual.Amplada - 1)
                    {
                        top(plataformaActual.Plataforma.Right);
                    }
                    else
                    {
                        top(plataformaActual.Plataforma.Center);
                    }
                    //Moure la posició d'spawn respecte al tamany del tile
                    posicioX += distancia;
                }
                break;
        }
    }
    public void interior(GameObject tileAPosar)
    {
        //Print de totes les posicions de tiles a la part inferior de la plataforma
        for(int i = 0; i < plataformaActual.Altura; i++)
        {
            Instantiate(tileAPosar, new Vector3(posicioX , positionBase.y + (distancia * i), 0), Quaternion.identity);
        }
    }
    public void top(GameObject tileAPosar)
    {
        //Print de la posició de tile de la plataforma
        Instantiate(tileAPosar, new Vector3(posicioX, positionBase.y+(distancia*plataformaActual.Altura), 0), Quaternion.identity);
    }
    
}
