﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsSpawnController : MonoBehaviour
{
    public float distancia = 0.6f;
    private List<GameObject> coins = new List<GameObject>();
    private float posicioX;
    private float posicioY;
    private float temps = 0;
    // Start is called before the first frame update
    void Start()
    {
        //Inicialtzar CoinSpawn al LevelManager
        LevelManager.Instance.initCoinSpawn();
        //Coordenada x del mon del limit dret de la pantalla + tile*1.5
        posicioX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x + distancia * 3;
        //Coordenada y del mon del limit inferior de la pantalla
        posicioY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
    }

    // Update is called once per frame
    void Update()
    {
        //Moure punt spawn respecte a temps transcorrgut i velocitat actual
        posicioX -= Time.deltaTime * LevelManager.Instance.Speed;
        //Afegir coins possibles
        addCoins();
        //Mirar si s'ha arribat al següent instant d'spawn
        if (temps <= 0)
        {
            //Mirar si hi ha coins possibles
            if (coins.Count != 0)
            {
                //Spawnear coins
                spawnCoins();
            }
            //Moure la posició d'spawn respecte al tamany del tile
            posicioX += distancia;
            //Temps d'espera a velocitat 1 per següent possible spawn
            temps += (distancia);
        }
        //Disminuir el temps respecte a temps transcorrgut i la velocitat actual
        temps -= Time.deltaTime * LevelManager.Instance.Speed;
        //Eliminar enemics possibles
        deleteCoins();
    }

    public void spawnCoins()
    {
        //Mirar si estic en plataforma
        if (LevelManager.Instance.AlturaPlataformaActual() != -20)
        {
            bool appear = false;
            //Disminuir espera d'aparició de tots els coins possibles
            foreach (GameObject coins in coins)
            {
                //Spawnear com a màxim 1 coin si ha acabat el seu temps d'espera
                if (coins.GetComponent<EnemyAppears>().wait <= 1 && !appear)
                {
                    spawnCoins(coins);
                    nextWaitForCoin(coins);
                    appear = true;
                }
                else
                {
                    coins.GetComponent<EnemyAppears>().wait -= 1;
                }
            }
        }
    }
    public void nextWaitForCoin(GameObject coin)
    {
        //Generar nou temps d'espera de la moneda
        EnemyAppears pCoin = coin.GetComponent<EnemyAppears>();
        pCoin.wait = Random.Range(pCoin.MinSeparation, pCoin.MaxSeparation + 1);
    }
    public void spawnCoins(GameObject coin)
    {
        //Spawnear la moneda
        Instantiate(coin, new Vector3(posicioX, posicioY + LevelManager.Instance.AlturaPlataformaActual() * distancia, 0), Quaternion.identity);
    }
    private void addCoins()
    {
        //Cercar si hi ha nous coins i afegir-los
        List<GameObject> newCoins = LevelManager.Instance.AddNewCoins();
        if (newCoins.Count != 0)
        {
            foreach (GameObject coin in newCoins)
            {
                coins.Add(coin);
            }

        }
    }
    private void deleteCoins()
    {
        //Eliminar coins que ja no han d'apareixer
        if (coins.Count != 0)
        {
            for (int i = 0; i < coins.Count; i++)
            {
                if (coins[i].GetComponent<EnemyAppears>().FinalDistance < LevelManager.Instance.Metres)
                {
                    coins.Remove(coins[i]);
                }

            }
        }
    }
}

