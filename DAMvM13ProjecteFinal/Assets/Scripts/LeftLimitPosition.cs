﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftLimitPosition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.position= new Vector2(Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x-2, transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
