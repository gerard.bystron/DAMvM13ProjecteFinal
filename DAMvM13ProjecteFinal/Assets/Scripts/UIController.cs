﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Image[] hearts;
    public Sprite heartFull, heartEmpty;
    public Text ScoreText, CoinsText;
    public Color HeartColor, ExtraHeartColor;
   
    // Update is called once per frame
    void Update()
    {
        ScoreText.text = "Distance:  " + LevelManager.Instance.Metres;
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < LevelManager.Instance.Lifes-3)
            {
                hearts[i].color = ExtraHeartColor;
            }
            else
            {
                hearts[i].color = HeartColor;
            }
            if (i < LevelManager.Instance.Lifes)
            {
                hearts[i].sprite = heartFull;
            }
            else
            {
                hearts[i].sprite = heartEmpty;
            }
        }
        CoinsText.text = "Coins:  " + LevelManager.Instance.Coin;
    }
}
