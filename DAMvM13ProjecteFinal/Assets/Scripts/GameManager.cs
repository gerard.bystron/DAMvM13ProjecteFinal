﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public float PowerUpGlassesTime = 3f, PowerUpBoostBulletTime = 3f, PowerUpJetpackTime = 3f;
    public int MaxCoins = 0,
        MaxDistance = 0,
        MaxScore = 0,
        MaxJumps = 0,
        MaxShots = 0,
        TotalCoins = 0,
        TotalDistance = 0,
        TotalScore = 0,
        TotalJumps = 0,
        TotalShots =0,
        KilledBoss = 0,
        KilledMushroom = 0,
        KilledBird = 0,
        TotalGames = 0,
        Coins = 0;
    public int LifeLevel = 0,
        GlassesLevel = 0,
        JetpackLevel = 0,
        MultiBulletLevel=0;
    public int Lifes = 3;
    //

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        PowerUpGlassesTime = PlayerPrefs.GetFloat("PowerUpGlassesTime", PowerUpGlassesTime);
        PowerUpBoostBulletTime = PlayerPrefs.GetFloat("PowerUpBoostBulletTime", PowerUpBoostBulletTime);
        PowerUpJetpackTime = PlayerPrefs.GetFloat("PowerUpJetpackTime", PowerUpJetpackTime);
        AudioListener.volume = PlayerPrefs.GetFloat("Volume", AudioListener.volume);
        MaxCoins = PlayerPrefs.GetInt("MaxCoins", MaxCoins);
        MaxDistance = PlayerPrefs.GetInt("MaxDistance", MaxDistance);
        MaxScore = PlayerPrefs.GetInt("MaxScore", MaxScore);
        MaxJumps = PlayerPrefs.GetInt("MaxJumps", MaxJumps);
        MaxShots = PlayerPrefs.GetInt("MaxShots", MaxShots);
        TotalCoins = PlayerPrefs.GetInt("TotalCoins", TotalCoins);
        TotalDistance = PlayerPrefs.GetInt("TotalDistance", TotalDistance);
        TotalScore = PlayerPrefs.GetInt("TotalScore", TotalScore);
        TotalJumps = PlayerPrefs.GetInt("TotalJumps", TotalJumps);
        TotalShots = PlayerPrefs.GetInt("TotalShots", TotalShots);
        KilledBoss = PlayerPrefs.GetInt("KilledBoss", KilledBoss);
        KilledMushroom = PlayerPrefs.GetInt("KilledMushroom", KilledMushroom);
        KilledBird = PlayerPrefs.GetInt("KilledBird", KilledBird);
        TotalGames = PlayerPrefs.GetInt("TotalGames", TotalGames);
        Coins = PlayerPrefs.GetInt("Coins", Coins);
        LifeLevel = PlayerPrefs.GetInt("LifeLevel", LifeLevel);
        GlassesLevel = PlayerPrefs.GetInt("GlassesLevel", GlassesLevel);
        JetpackLevel = PlayerPrefs.GetInt("JetpackLevel", JetpackLevel);
        MultiBulletLevel = PlayerPrefs.GetInt("MultiBulletLevel", MultiBulletLevel);
        Lifes = PlayerPrefs.GetInt("Lifes", Lifes);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
