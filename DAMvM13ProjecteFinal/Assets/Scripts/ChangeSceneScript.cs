﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneScript : MonoBehaviour
{
    public void MenuLoader()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void SceneLoader()
    {
        SceneManager.LoadScene(3,LoadSceneMode.Single);
    }

    public void SettingsLoader()
    {
        SceneManager.LoadScene(4, LoadSceneMode.Single);
    }
    
    public void AchievementsLoader() {
        SceneManager.LoadScene(5, LoadSceneMode.Single);
    }
    public void ShopLoader()
    {
        SceneManager.LoadScene(6, LoadSceneMode.Single);
    }
    public void IntroLoader()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    public void Exit()
    {
        Save();
        Application.Quit();
    }
    public void Save()
    {
        PlayerPrefs.SetFloat("PowerUpGlassesTime",GameManager.Instance.PowerUpGlassesTime);
        PlayerPrefs.SetFloat("PowerUpBoostBulletTime", GameManager.Instance.PowerUpBoostBulletTime);
        PlayerPrefs.SetFloat("PowerUpJetpackTime", GameManager.Instance.PowerUpJetpackTime);
        PlayerPrefs.SetFloat("Volume", AudioListener.volume);
        PlayerPrefs.SetInt("MaxCoins", GameManager.Instance.MaxCoins);
        PlayerPrefs.SetInt("MaxDistance", GameManager.Instance.MaxDistance);
        PlayerPrefs.SetInt("MaxScore", GameManager.Instance.MaxScore);
        PlayerPrefs.SetInt("MaxJumps", GameManager.Instance.MaxJumps);
        PlayerPrefs.SetInt("MaxShots", GameManager.Instance.MaxShots);
        PlayerPrefs.SetInt("TotalCoins", GameManager.Instance.TotalCoins);
        PlayerPrefs.SetInt("TotalDistance", GameManager.Instance.TotalDistance);
        PlayerPrefs.SetInt("TotalScore", GameManager.Instance.TotalScore);
        PlayerPrefs.SetInt("TotalJumps", GameManager.Instance.TotalJumps);
        PlayerPrefs.SetInt("TotalShots", GameManager.Instance.TotalShots);
        PlayerPrefs.SetInt("KilledBoss", GameManager.Instance.KilledBoss);
        PlayerPrefs.SetInt("KilledMushroom", GameManager.Instance.KilledMushroom);
        PlayerPrefs.SetInt("KilledBird", GameManager.Instance.KilledBird);
        PlayerPrefs.SetInt("TotalGames", GameManager.Instance.TotalGames);
        PlayerPrefs.SetInt("Coins", GameManager.Instance.Coins);
        PlayerPrefs.SetInt("LifeLevel", GameManager.Instance.LifeLevel);
        PlayerPrefs.SetInt("GlassesLevel", GameManager.Instance.GlassesLevel);
        PlayerPrefs.SetInt("JetpackLevel", GameManager.Instance.JetpackLevel);
        PlayerPrefs.SetInt("MultiBulletLevel", GameManager.Instance.MultiBulletLevel);
        PlayerPrefs.SetInt("Lifes", GameManager.Instance.Lifes);
        PlayerPrefs.Save();
    }
}
