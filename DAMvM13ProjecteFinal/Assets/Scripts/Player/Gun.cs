﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject BulletPrefab, Player, bullet;
    public Transform gun, shotpoint;
    private Rigidbody2D bulletRB;
    private Camera cam;
    private Vector2 aim;
    public float speed = 30f, maxAngle = 100f;
    public int numBales = 1;
    public AudioSource source;
    public AudioClip shootSound;
   
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        shotpoint = GameObject.Find("ShotPoint").GetComponent<Transform>();
        gun = GameObject.Find("Gun").GetComponent<Transform>();
        Player = GameObject.Find("Player");
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            //Get the mouse Point
            Vector2 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
            //Get the vector between mousePos and Player position
            aim = mousePos - Player.GetComponent<Rigidbody2D>().position;
            //Angle between each other
            float angle = Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg;

            //Condition to limit how to shot
            if (angle > -maxAngle)
            {
                if (angle < maxAngle)
                {
                }
                else
                {
                    angle = maxAngle;
                }
            }
            else {
                angle = -maxAngle;
            }
            //Rotates the gun into the point where player shot!
            gun.rotation = Quaternion.Euler(0f, 0f, angle);
            //Bullet got instantiated in world
            numBales = LevelManager.Instance.bullets;
            Shoot(numBales, angle);
        }
    }
    void Shoot(int nBullets, float angle)
    {
        source.PlayOneShot(shootSound, 1);

        for (int i = 1; i <= nBullets; i++)
        {
            LevelManager.Instance.Shots++;
            bullet = Instantiate(BulletPrefab, shotpoint.position, Quaternion.Euler(0f, 0f, angle));
            //We get the RB of the bullet to add the force
            bulletRB = bullet.GetComponent<Rigidbody2D>();
            //Add force to position
            switch (i)
            {
                case 1:
                    bulletRB.AddRelativeForce(new Vector2(1, 0) * speed, ForceMode2D.Force);
                    break;
                case 2:
                    bulletRB.AddRelativeForce(new Vector2(1, -0.5f) * speed, ForceMode2D.Force);
                    break;
                case 3:
                    bulletRB.AddRelativeForce(new Vector2(1, 0.5f) * speed, ForceMode2D.Force);
                    break;
                case 4:
                    bulletRB.AddRelativeForce(new Vector2(1, 0.75f) * speed, ForceMode2D.Force);
                    break;
                case 5:
                    bulletRB.AddRelativeForce(new Vector2(1, -0.75f) * speed, ForceMode2D.Force);
                    break;
                case 6:
                    bulletRB.AddRelativeForce(new Vector2(1, 0.25f) * speed, ForceMode2D.Force);
                    break;
                case 7:
                    bulletRB.AddRelativeForce(new Vector2(1, -0.25f) * speed, ForceMode2D.Force);
                    break;
                case 8:
                    bulletRB.AddRelativeForce(new Vector2(1, 1f) * speed, ForceMode2D.Force);
                    break;
                case 9:
                    bulletRB.AddRelativeForce(new Vector2(1, -1f) * speed, ForceMode2D.Force);
                    break;
                default:
                    break;
            }
        }
    } 
}
