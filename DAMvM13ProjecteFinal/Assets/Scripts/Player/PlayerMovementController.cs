﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    private Vector3 posInicial, posActual;
    public float speed, jumpForce, numJumps, maxJumps = 2;
    private Transform playerTransform;
    private bool isGrounded;
    private Rigidbody2D playerRB;
    private Collider2D playerCollider;
    private Animator monkeyAnim, coinAnim;
    //AUDIO STUFF
    public AudioClip jumpAudio;
    public AudioClip flyAudio;
    public AudioClip pickCoinAudio;
    public AudioClip audioHit;
    public AudioClip pickItemAudio;
    public AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
        //START AUDIO
        //  source = GetComponent<AudioSource>();
        transform.position = new Vector2(Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/7, 0, 0)).x, transform.position.y);
        monkeyAnim = gameObject.GetComponent<Animator>();
        playerRB = gameObject.GetComponent<Rigidbody2D>();
        playerTransform = gameObject.GetComponent<Transform>();
        posInicial = playerTransform.transform.position;
        speed = LevelManager.Instance.Speed + 1f;
        numJumps = 0f;

    }

    // Update is called once per frame
    void Update()
    {
        posActual = playerTransform.transform.position;
        MoveToInitialPoint();
        if (!LevelManager.Instance.HasJetPack) { Jump(); } else { Fly(); }
    }

    void MoveToInitialPoint(){
        if (posActual.x < posInicial.x)
        {
            playerTransform.transform.position = playerTransform.transform.position + new Vector3(1 * speed * Time.deltaTime, 0 * speed * Time.deltaTime);
        } else if (posActual.x > posInicial.x)
        {
            playerTransform.transform.position = playerTransform.transform.position + new Vector3(-1 * speed * Time.deltaTime, 0 * speed * Time.deltaTime);
        }
    }
    void Jump()
    {
        
        if (numJumps >= maxJumps) {
            isGrounded = false;
        }
        //Debug.Log(isGrounded);
        if (isGrounded) {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                playerRB.velocity = Vector2.zero;
                source.PlayOneShot(jumpAudio, 1);
                numJumps++;
                playerRB.AddForce(Vector3.up * jumpForce);
                monkeyAnim.SetBool("jump", true);
                LevelManager.Instance.Jumps++;
            }
        }
    }

    void Fly()
    {
        playerRB.velocity = Vector2.zero;
        monkeyAnim.SetBool("jump", true);
        if (posActual.y < 2.5)
        {
            playerTransform.position = Vector3.MoveTowards(posActual, new Vector3(posActual.x, 2.5f), 3 * Time.deltaTime);
            playerRB.gravityScale = 0;
        }
        else if(posActual.y > 2.5){
            playerTransform.position = Vector3.MoveTowards(posActual, new Vector3(posActual.x, 2.5f), -3 * Time.deltaTime);
        } //playerRB.AddForce(Vector3.up * 1.1f); //else playerRB.transform.Translate(Vector3.up * 1.1f); transform.Translate(0, Time.deltaTime*2, 0, Space.World)
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Platform")
        {
           if (collision.transform.position.y + 0.7f < playerTransform.position.y)
            {
                isGrounded = true;
                monkeyAnim.SetBool("jump", false);
                numJumps = 0;
            }
        }
        if (collision.gameObject.tag == "Enemy")
        {
            // Player hit an enemy...(Player should lose life here)
            gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            StartCoroutine(timeHit());
            source.PlayOneShot(audioHit, 1);
            LevelManager.Instance.Lifes -= 1;
            Debug.Log("lives"+ LevelManager.Instance.Lifes);
            Destroy(collision.gameObject);
        }
    }
    IEnumerator timeHit() 
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
    //Trigger Collisions between Player and stuff
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeathLimit")) LevelManager.Instance.PlayerDead(false);

        if (collision.CompareTag("Coin"))
        {
            source.PlayOneShot(pickCoinAudio, 1);
            LevelManager.Instance.Coin += 1;
            coinAnim = collision.GetComponent<Animator>();
            coinAnim.SetBool("isTaken", true);
        }
        if (collision.CompareTag("Heart"))
        {
            source.PlayOneShot(pickItemAudio, 1);
            if (LevelManager.Instance.Lifes < LevelManager.Instance.MaxLifes)
            {
                LevelManager.Instance.Lifes += 1;
            }
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("BadCoin"))
        {
            if (LevelManager.Instance.Coin >= 1)
            {
                source.PlayOneShot(audioHit, 1);
                LevelManager.Instance.Coin -= 1;
                coinAnim = collision.GetComponent<Animator>();
                coinAnim.SetBool("isTaken", true);
            }
        }

        if (collision.CompareTag("Glasses"))
        {
            source.PlayOneShot(pickItemAudio, 1);
            LevelManager.Instance.HasGlasses = true;
            StartCoroutine(LevelManager.Instance.powerupTime());
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("BananaPack"))
        {
            source.PlayOneShot(pickItemAudio, 1);
            LevelManager.Instance.HasJetPack = true;
            LevelManager.Instance.jetpack.SetActive(true);
            StartCoroutine(LevelManager.Instance.powerupTime2());
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("BoostBullet"))
        {
            source.PlayOneShot(pickItemAudio, 1);
            LevelManager.Instance.bullets += 2;
            StartCoroutine(LevelManager.Instance.powerupTime3());
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("Enemy"))
        {
            LevelManager.Instance.Lifes -= 1;
            Debug.Log("lives" + LevelManager.Instance.Lifes);
            Destroy(collision.gameObject);
        }
    }
        
}
