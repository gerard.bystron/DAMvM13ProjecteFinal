﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroyer : MonoBehaviour

{
    private Animator enemyAnim;

    void Awake()
    {

        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BorderBullets")
        {
            Destroy(gameObject);
        }
    }
    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Platform" )
        {
            Destroy(gameObject);

        }else if (collision.gameObject.tag == "Enemy")
        {
            LevelManager.Instance.enemyDeathSound();
            GameManager.Instance.KilledMushroom++;
            enemyAnim = collision.gameObject.GetComponent<Animator>();
            enemyAnim.SetBool("dead", true);
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "Boss")
        {
            
            collision.gameObject.GetComponent<Boss>().lostLife();
            Destroy(gameObject);
        } else if (collision.gameObject.tag == "EnemyBird")
        {
            LevelManager.Instance.enemyDeathSoundBird();
            GameManager.Instance.KilledBird++;
            enemyAnim = collision.gameObject.GetComponent<Animator>();
            enemyAnim.SetBool("dead", true);
            Destroy(gameObject);
        }


    }
}
