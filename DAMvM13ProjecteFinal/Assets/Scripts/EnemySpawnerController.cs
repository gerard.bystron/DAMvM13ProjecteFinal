﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerController : MonoBehaviour
{
    public float distancia = 0.6f;
    private List<GameObject> enemies=new List<GameObject>();
    private float posicioX;
    private float posicioY;
    // Start is called before the first frame update
    void Start()
    {
        //Inicialtzar EnemySpawn al LevelManager
        LevelManager.Instance.initEnemySpawn();
        //Coordenada x del mon del limit dret de la pantalla + tile*1.5
        posicioX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x+ distancia*3f;
        //Coordenada y del mon del limit inferior de la pantalla
        posicioY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
    }
    private float temps=0;

    // Update is called once per frame
    void Update()
    {
        //Moure punt spawn respecte a temps transcorrgut i velocitat actual
        posicioX -= Time.deltaTime * LevelManager.Instance.Speed;
        //Afegir enemics possibles
        addEnemies();
        //Mirar si s'ha arribat al següent instant d'spawn
        if (temps <= 0)
        {
            //Mirar si hi ha enemics possibles
            if (enemies.Count!=0)
            {
                //Spawnear enemics
                spawnEnemies();
            }
            //Moure la posició d'spawn respecte al tamany del tile
            posicioX += distancia;
            //Temps d'espera a velocitat 1 per següent possible spawn
            temps += (distancia);
        }
        //Disminuir el temps respecte a temps transcorrgut i la velocitat actual
        temps -= Time.deltaTime * LevelManager.Instance.Speed;
        //Eliminar enemics possibles
        deleteEnemies();
    }
    public void spawnEnemies()
    {
        //Mirar si estic en plataforma
        if (LevelManager.Instance.AlturaPlataformaActual() != -20)
        {
            bool appear = false;
            //Disminuir espera d'aparició de tots els enemics possibles
            foreach (GameObject enemy in enemies)
            {
                //Spawnear com a màxim 1 enemic si ha acabat el seu temps d'espera
                if (enemy.GetComponent<EnemyAppears>().wait <=1 && !appear)
                {
                    spawnEnemy(enemy);
                    nextWaitForEnemy(enemy);
                    appear = true;
                }
                else
                {
                    enemy.GetComponent<EnemyAppears>().wait -= 1;
                }
            }
        }
    }
    public void nextWaitForEnemy(GameObject enemy)
    {
        //Generar nou temps d'espera de l'enemic
        EnemyAppears pEnemy = enemy.GetComponent<EnemyAppears>();
        pEnemy.wait = Random.Range(pEnemy.MinSeparation,pEnemy.MaxSeparation+1);
    }
    public void spawnEnemy(GameObject enemy)
    {
        //Spawnear l'enemic
        Instantiate(enemy, new Vector3(posicioX, posicioY + LevelManager.Instance.AlturaPlataformaActual() * distancia, 0), Quaternion.identity);
    }
    private void addEnemies()
    {
        //Cercar si hi ha nous enemics i afegir-los
        List<GameObject> newEnemies = LevelManager.Instance.AddNewEnemies();
        if (newEnemies.Count != 0)
        {
            foreach (GameObject enemy in newEnemies)
            {
                enemies.Add(enemy);
            }

        }
    }
    private void deleteEnemies()
    {
        //Eliminar enemics que ja no han d'apareixer
        if (enemies.Count != 0)
        {
            for (int i=0; i<enemies.Count;i++ )
            { 
                if (enemies[i].GetComponent<EnemyAppears>().FinalDistance < LevelManager.Instance.Metres)
                {
                    enemies.Remove(enemies[i]);
                }

            }
        }
    }
}
