﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wilberforce;

public class ColorblindManager : MonoBehaviour
{
    public Colorblind Colorblind; 
    // Start is called before the first frame update
    void Start()
    {
        Colorblind = gameObject.GetComponent<Colorblind>();
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelManager.Instance.HasGlasses || LevelManager.Instance.Metres <= 50) {
            Colorblind.Type = 0;
            
        }else if (LevelManager.Instance.Metres % 150 >= 100 && LevelManager.Instance.Metres % 150 <= 150)
        {
            Colorblind.Type = 3;
        }
        else if (LevelManager.Instance.Metres % 150 >= 50 && LevelManager.Instance.Metres % 150 <= 100)
        {
            Colorblind.Type = 2;
        }
        else if (LevelManager.Instance.Metres % 150 >= 0 && LevelManager.Instance.Metres % 150 <= 50)
        {
            Colorblind.Type = 1;
        }
    }
}

