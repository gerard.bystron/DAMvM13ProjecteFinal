﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(Plataforma))]
public class PlataformaEditor :Editor
{
    public SerializedProperty
        tipus1_Prop,
        tipus2_Prop,

        tileTopLeft_Prop,
        tileTop_Prop,
        tileTopRight_Prop,
        tileLeft_Prop,
        tileCenter_Prop,
        tileRight_Prop,
        tileBottomLeft_Prop,
        tileBottom_Prop,
        tileBottomRight_Prop,

        minCanviAlturaAmunt_Prop,
        maxCanviAlturaAmunt_Prop,
        minCanviAlturaAvall_Prop,
        maxCanviAlturaAvall_Prop,
        minDifAlturaABase_Prop,
        maxDifAlturaABase_Prop,

        minAltura_Prop,
        maxAltura_Prop,
        minAmplada_Prop,
        maxAmplada_Prop;

    void OnEnable()
    {
        // Setup the SerializedProperties
        tipus1_Prop = serializedObject.FindProperty("tipus1");
        tipus2_Prop = serializedObject.FindProperty("tipus2");

        tileTopLeft_Prop = serializedObject.FindProperty("TopLeft");
        tileTop_Prop = serializedObject.FindProperty("Top");
        tileTopRight_Prop = serializedObject.FindProperty("TopRight");
        tileLeft_Prop = serializedObject.FindProperty("Left");
        tileCenter_Prop = serializedObject.FindProperty("Center");
        tileRight_Prop = serializedObject.FindProperty("Right");
        tileBottomLeft_Prop = serializedObject.FindProperty("BottomLeft");
        tileBottom_Prop = serializedObject.FindProperty("Bottom");
        tileBottomRight_Prop = serializedObject.FindProperty("BottomRight");

        minCanviAlturaAmunt_Prop = serializedObject.FindProperty("MinCanviAlturaAmunt");
        maxCanviAlturaAmunt_Prop = serializedObject.FindProperty("MaxCanviAlturaAmunt");
        minCanviAlturaAvall_Prop = serializedObject.FindProperty("MinCanviAlturaAvall");
        maxCanviAlturaAvall_Prop = serializedObject.FindProperty("MaxCanviAlturaAvall");
        minDifAlturaABase_Prop   = serializedObject.FindProperty("MinDifAlturaABase");
        maxDifAlturaABase_Prop   = serializedObject.FindProperty("MaxDifAlturaABase");

        minAltura_Prop = serializedObject.FindProperty("MinAltura");
        maxAltura_Prop = serializedObject.FindProperty("MaxAltura");
        minAmplada_Prop = serializedObject.FindProperty("MinAmplada");
        maxAmplada_Prop = serializedObject.FindProperty("MaxAmplada");
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        //hola_Prop.intValue = EditorGUILayout.IntField("Hola", hola_Prop.intValue);
        

        EditorGUILayout.PropertyField(tipus1_Prop);
        Plataforma.type1 st = (Plataforma.type1)tipus1_Prop.intValue;

        EditorGUILayout.PropertyField(tipus2_Prop);
        Plataforma.type2 st1 = (Plataforma.type2)tipus2_Prop.intValue;

        minAltura_Prop.intValue = EditorGUILayout.IntField("MinAltura", minAltura_Prop.intValue);
        maxAltura_Prop.intValue = EditorGUILayout.IntField("MaxAltura", maxAltura_Prop.intValue);
        minAmplada_Prop.intValue = EditorGUILayout.IntField("MinAmplada", minAmplada_Prop.intValue);
        maxAmplada_Prop.intValue = EditorGUILayout.IntField("MaxAmplada", maxAmplada_Prop.intValue);

        switch (st)
        {
            case Plataforma.type1.Base:
                minCanviAlturaAmunt_Prop.intValue = EditorGUILayout.IntField("MinCanviAlturaAmunt",  minCanviAlturaAmunt_Prop.intValue);
                maxCanviAlturaAmunt_Prop.intValue = EditorGUILayout.IntField("MaxCanviAlturaAmunt",  maxCanviAlturaAmunt_Prop.intValue);
                minCanviAlturaAvall_Prop.intValue = EditorGUILayout.IntField("MinCanviAlturaAvall",  minCanviAlturaAvall_Prop.intValue);
                maxCanviAlturaAvall_Prop.intValue = EditorGUILayout.IntField("MaxCanviAlturaAvall",  maxCanviAlturaAvall_Prop.intValue);
                break;

            case Plataforma.type1.Extra1:
                minDifAlturaABase_Prop.intValue = EditorGUILayout.IntField("MinDifAlturaABase",  minDifAlturaABase_Prop.intValue);
                maxDifAlturaABase_Prop.intValue = EditorGUILayout.IntField("MaxDifAlturaABase", maxDifAlturaABase_Prop.intValue);
                break;

        }
        

        switch (st1)
        {
            case Plataforma.type2.Normal1:
                tileTopLeft_Prop.objectReferenceValue=EditorGUILayout.ObjectField("TopLeft",tileTopLeft_Prop.objectReferenceValue,typeof(GameObject),false);
                tileTop_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Top", tileTop_Prop.objectReferenceValue, typeof(GameObject), false);
                tileTopRight_Prop.objectReferenceValue = EditorGUILayout.ObjectField("TopRight", tileTopRight_Prop.objectReferenceValue, typeof(GameObject), false);
                tileLeft_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Left", tileLeft_Prop.objectReferenceValue, typeof(GameObject), false);
                tileCenter_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Center", tileCenter_Prop.objectReferenceValue, typeof(GameObject), false);
                tileRight_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Right", tileRight_Prop.objectReferenceValue, typeof(GameObject), false);
                break;

            case Plataforma.type2.Flotant1:
                tileLeft_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Left", tileLeft_Prop.objectReferenceValue, typeof(GameObject), false);
                tileCenter_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Center", tileCenter_Prop.objectReferenceValue, typeof(GameObject), false);
                tileRight_Prop.objectReferenceValue = EditorGUILayout.ObjectField("Right", tileRight_Prop.objectReferenceValue, typeof(GameObject), false);
                break;

        }


        serializedObject.ApplyModifiedProperties();
    }

}
